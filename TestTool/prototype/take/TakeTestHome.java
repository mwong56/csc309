package take;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;

import create.MenuBarFactory;
import proctor.AnswerStudentQuestion;

public class TakeTestHome extends JFrame {

  private static final long serialVersionUID = 1L;

  public TakeTestHome() {
    initUI();
  }

  private void initUI() {
    setTitle("Take Test");
    setSize(700, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);

    setJMenuBar(MenuBarFactory.createMenuStudent(this));
    createTable();
    createButtons();
  }

  private void createTable() {
    String[] columnNames = { "Class", "Professor", "Test", "Type", "Close", "Time Limit", "Take" };
    Object[][] data = {
        { "CSC 102", "S. Werbenjagermanjensen", "Practice Midterm #1", "Practice Exam", "2/6/15",
            "N/A", "Take" },
        { "CSC 102", "S. Werbenjagermanjensen", "Midterm #1", "Take-Home Exam", "2/8/15", "2 hrs",
            "Take" },
        { "CSC 102", "S. Werbenjagermanjensen", "Midterm #2", "Proctored Exam", "2/10/15", "2 hrs",
            "Take" }, };

    JTable table = new JTable(data, columnNames);
    table.getColumn("Take").setCellRenderer(new ButtonRenderer());
    table.getColumn("Take").setCellEditor(new ButtonEditor(new JCheckBox()));
    JScrollPane scrollPane = new JScrollPane(table);
    add(scrollPane, BorderLayout.CENTER);
    setVisible(true);

  }

  class ButtonRenderer extends JButton implements TableCellRenderer {

    private static final long serialVersionUID = 1L;

    public ButtonRenderer() {
      setOpaque(true);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
        boolean hasFocus, int row, int column) {
      if (isSelected) {
        setForeground(table.getSelectionForeground());
        setBackground(table.getSelectionBackground());
      } else {
        setForeground(table.getForeground());
        setBackground(UIManager.getColor("Button.background"));
      }
      setText((value == null) ? "" : value.toString());
      return this;
    }
  }

  class ButtonEditor extends DefaultCellEditor {
    /**
       * 
       */
    private static final long serialVersionUID = 1L;
    protected JButton button;
    private String label;
    private String question;
    private String answer;
    private boolean isPushed;

    public ButtonEditor(JCheckBox checkBox) {
      super(checkBox);
      button = new JButton();
      button.setOpaque(true);
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
        int row, int column) {
      if (isSelected) {
        button.setForeground(table.getSelectionForeground());
        button.setBackground(table.getSelectionBackground());
      } else {
        button.setForeground(table.getForeground());
        button.setBackground(table.getBackground());
      }
      question = (String) table.getValueAt(row, 1);
      answer = (String) table.getValueAt(row, 2);
      label = (value == null) ? "" : value.toString();
      button.setText(label);
      isPushed = true;
      return button;
    }

    public Object getCellEditorValue() {
      if (isPushed) {
        AnswerStudentQuestion view = new AnswerStudentQuestion(question, answer);
        view.setVisible(true);
        setVisible(false);
      }
      isPushed = false;
      return new String(label);
    }

    public boolean stopCellEditing() {
      isPushed = false;
      return super.stopCellEditing();
    }

    protected void fireEditingStopped() {
      super.fireEditingStopped();
    }
  }

  private void createButtons() {
    JButton editButton = new JButton("Add");
    setMaxSize(editButton);
    editButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        System.out.println("Add Question...");
        System.exit(0);
      }
    });

    JButton deleteButton = new JButton("Delete");
    setMaxSize(deleteButton);
    deleteButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        System.out.println("Deleted Question!");
        System.exit(0);
      }
    });

    createLayout(editButton, deleteButton);
  }

  private void createLayout(JComponent... arg) {

    Container pane = getContentPane();
    GroupLayout questionBank = new GroupLayout(pane);
    pane.setLayout(questionBank);
    questionBank.setAutoCreateGaps(true);
    questionBank.setAutoCreateContainerGaps(true);
  }

  private void setMaxSize(JComponent jc) {
    Dimension max = jc.getMaximumSize();
    Dimension pref = jc.getPreferredSize();
    max.height = pref.height;
    max.width = pref.width;
    jc.setMaximumSize(max);
  }

}
