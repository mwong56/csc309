package create;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import admin.AdminMain;
import main.Main;
import proctor.ProctorTest;
import questions.QuestionBank;
import take.TakeTestHome;

public class MenuBarFactory {

  // Adds Menus to the JMenuBar
  private static void addMenus(JMenuBar menuBar, JMenu[] menus) {
    for (int i = 0; i < menus.length; i++) {
      menuBar.add(menus[i]);
    }
  }

  // Creates the menu bar
  public static JMenuBar createMenu(final JFrame instance) {
    JMenuBar menuBar = new JMenuBar();
    JMenu home = new JMenu("Home");
    home.addMenuListener(new MenuListener() {

      @Override
      public void menuSelected(MenuEvent e) {
        Main home = new Main();
        home.setVisible(true);
        instance.setVisible(false);
      }

      @Override
      public void menuDeselected(MenuEvent e) {
        // TODO Auto-generated method stub

      }

      @Override
      public void menuCanceled(MenuEvent e) {
        // TODO Auto-generated method stub

      }
    });

    JMenu questions = new JMenu("Questions");
    questions.addMenuListener(new MenuListener() {

      @Override
      public void menuSelected(MenuEvent e) {
        QuestionBank ex = new QuestionBank();
        ex.setVisible(true);
        instance.setVisible(false);
      }

      @Override
      public void menuDeselected(MenuEvent e) {
        // TODO Auto-generated method stub

      }

      @Override
      public void menuCanceled(MenuEvent e) {
        // TODO Auto-generated method stub

      }
    });
    JMenu create = new JMenu("Create Test");
    create.addMenuListener(new MenuListener() {

      @Override
      public void menuSelected(MenuEvent e) {
        CreateTest ct = new CreateTest();
        ct.setVisible(true);
        instance.setVisible(false);
      }

      @Override
      public void menuDeselected(MenuEvent e) {
        // TODO Auto-generated method stub

      }

      @Override
      public void menuCanceled(MenuEvent e) {
        // TODO Auto-generated method stub

      }
    });

    JMenu proctor = new JMenu("Proctor Test");
    proctor.addMenuListener(new MenuListener() {

      @Override
      public void menuSelected(MenuEvent e) {
        ProctorTest test = new ProctorTest();
        test.setVisible(true);
        instance.setVisible(false);
      }

      @Override
      public void menuDeselected(MenuEvent e) {
        // TODO Auto-generated method stub

      }

      @Override
      public void menuCanceled(MenuEvent e) {
        // TODO Auto-generated method stub

      }
    });

    JMenu review = new JMenu("Review Test");
    review.addMenuListener(new MenuListener() {

      @Override
      public void menuSelected(MenuEvent e) {
        // TODO Auto-generated method stub

      }

      @Override
      public void menuDeselected(MenuEvent e) {
        // TODO Auto-generated method stub

      }

      @Override
      public void menuCanceled(MenuEvent e) {
        // TODO Auto-generated method stub

      }
    });

    JMenu admin = new JMenu("Admin");
    admin.addMenuListener(new MenuListener() {

      @Override
      public void menuSelected(MenuEvent e) {
        AdminMain ai = new AdminMain();
        ai.setVisible(true);
        instance.setVisible(false);

      }

      @Override
      public void menuDeselected(MenuEvent e) {
        // TODO Auto-generated method stub

      }

      @Override
      public void menuCanceled(MenuEvent e) {
        // TODO Auto-generated method stub

      }
    });

    JMenu[] menus = { home, questions, create, proctor, review, admin };

    addMenus(menuBar, menus);

    return menuBar;
  }

  public static JMenuBar createMenuStudent(final JFrame instance) {
    JMenuBar menuBar = new JMenuBar();
    JMenu home = new JMenu("Home");
    home.addMenuListener(new MenuListener() {

      @Override
      public void menuSelected(MenuEvent e) {
        Main home = new Main();
        home.setVisible(true);
        instance.setVisible(false);
      }

      @Override
      public void menuDeselected(MenuEvent e) {
        // TODO Auto-generated method stub

      }

      @Override
      public void menuCanceled(MenuEvent e) {
        // TODO Auto-generated method stub

      }
    });

    JMenu take = new JMenu("Take Test");
    take.addMenuListener(new MenuListener() {

      @Override
      public void menuSelected(MenuEvent e) {
        TakeTestHome test = new TakeTestHome();
        test.setVisible(true);
        instance.setVisible(false);
      }

      @Override
      public void menuDeselected(MenuEvent e) {
        // TODO Auto-generated method stub

      }

      @Override
      public void menuCanceled(MenuEvent e) {
        // TODO Auto-generated method stub

      }
    });

    JMenu review = new JMenu("Review Test");
    review.addMenuListener(new MenuListener() {

      @Override
      public void menuSelected(MenuEvent e) {
        // TODO Auto-generated method stub
      }

      @Override
      public void menuDeselected(MenuEvent e) {
        // TODO Auto-generated method stub

      }

      @Override
      public void menuCanceled(MenuEvent e) {
        // TODO Auto-generated method stub

      }
    });

    JMenu[] menus = { home, take, review };

    addMenus(menuBar, menus);

    return menuBar;
  }

}
