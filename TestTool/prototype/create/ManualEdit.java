package create;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;

public class ManualEdit extends JFrame {
  private static final long serialVersionUID = 1L;
  private String subject;
  private String test;

  public ManualEdit(String subject, String test) {
    this.subject = subject;
    this.test = test;
    initUI();
    createTable();
  }

  private void initUI() {
    setTitle("Manual Edit" + " (" + subject + " - " + test + ")");
    setSize(700, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setJMenuBar(MenuBarFactory.createMenu(this));
  }

  private void createTable() {
    String[] columnNames = { "Question Number", "Difficulty", " Type", "Question", "Replace" };
    Object[][] data = { { "1", "1", "M/C", "How to declare a variable?", "Replace" },
        { "2", "3", "Fill-In", "What is the output of this program?", "Replace" } };

    JTable table = new JTable(data, columnNames);
    table.getColumn("Replace").setCellRenderer(new ButtonRenderer());
    JScrollPane scrollPane = new JScrollPane(table);
    add(scrollPane, BorderLayout.CENTER);
    setVisible(true);
  }

  class ButtonRenderer extends JButton implements TableCellRenderer {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ButtonRenderer() {
      setOpaque(true);
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
        boolean hasFocus, int row, int column) {
      if (isSelected) {
        setForeground(table.getSelectionForeground());
        setBackground(table.getSelectionBackground());
      } else {
        setForeground(table.getForeground());
        setBackground(UIManager.getColor("Button.background"));
      }
      setText((value == null) ? "" : value.toString());
      return this;
    }
  }

  class ButtonEditor extends DefaultCellEditor {
    private static final long serialVersionUID = 1L;
    protected JButton button;
    private String label;
    private boolean isPushed;

    public ButtonEditor(JCheckBox checkBox) {
      super(checkBox);
      button = new JButton();
      button.setOpaque(true);
      button.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          fireEditingStopped();
        }
      });
    }

    public Object getCellEditorValue() {
      if (isPushed) {
        // changes question in test
        setVisible(false);
      }
      isPushed = false;
      return new String(label);
    }

    public boolean stopCellEditing() {
      isPushed = false;
      return super.stopCellEditing();
    }

    protected void fireEditingStopped() {
      super.fireEditingStopped();
    }
  }
}
