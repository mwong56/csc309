package questions;
import static javax.swing.GroupLayout.Alignment.LEADING;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import main.Main;
import create.MenuBarFactory;

public class ShortAnswerQuestion extends JPanel {
  GroupLayout layout;
  
  private static final long serialVersionUID = 1L;

  public ShortAnswerQuestion() {
	layout = new GroupLayout(this);
	setLayout(layout);
	createLayout();
  }

  private void createLayout(JComponent... arg) {
    // Question
	JLabel question = new JLabel("Question");
	JTextField questionBox = new JTextField();
	setMaxSize(questionBox, 500, 500);

	// Answer Notes
	JLabel answerNotes = new JLabel("Answer Notes");
	JTextField answerNotesBox = new JTextField();
	setMaxSize(answerNotesBox, 500, 500);
	
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    
 // Horizontal Formatting
    layout.setHorizontalGroup(layout
        .createSequentialGroup()
        .addGroup(
        	layout.createParallelGroup()
        	.addComponent(question)
	        .addComponent(questionBox)
	        .addComponent(answerNotes)
	        .addComponent(answerNotesBox)));

    // Vertical Formatting
    layout.setVerticalGroup(layout
        .createSequentialGroup()
        .addComponent(question)
        .addComponent(questionBox)
        .addComponent(answerNotes)
        .addComponent(answerNotesBox));
  }

  private void setMaxSize(JComponent jc) {
    Dimension max = jc.getMaximumSize();
    Dimension pref = jc.getPreferredSize();
    max.height = pref.height;
    max.width = pref.width;
    jc.setMaximumSize(max);
  }

  private void setMaxSize(JComponent jc, int x, int y) {
    Dimension max = jc.getMaximumSize();
    max.height = y;
    max.width = x;
    jc.setMaximumSize(max);
  }
}
