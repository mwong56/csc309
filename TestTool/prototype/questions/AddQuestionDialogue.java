package questions;
import static javax.swing.GroupLayout.Alignment.LEADING;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.ItemSelectable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import create.MenuBarFactory;

public class AddQuestionDialogue extends JFrame implements ItemListener {
	JPanel cards; //a panel that uses CardLayout
	final static String NONE = "-- none selected --";
    final static String TRUEFALSE = "True/False";
    final static String FILLINTHEBLANK = "Fill-in-the-Blank";
    final static String SHORTANSWER = "Short Answer";
    final static String LONGANSWER = "Long Answer";
    
  private static final long serialVersionUID = 1L;

  public AddQuestionDialogue() {
    initUI();
  }

  private void initUI() {
    setTitle("Add a Question");
    setSize(500, 700);
    setLocationRelativeTo(null);

    createButtons();
  }

  private void createButtons() {
    JButton addButton = new JButton("Add");
    setMaxSize(addButton);
    addButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        System.out.println("Added Question!\n");
        setVisible(false);
      }
    });

    JButton cancelButton = new JButton("Cancel");
    setMaxSize(cancelButton);
    cancelButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        System.out.println("Cancelled\n");
        setVisible(false);
      }
    });

    createLayout(addButton, cancelButton);
  }

  private void createLayout(JComponent... arg) {

    Container pane = getContentPane();
    GroupLayout simpleAddQuestion = new GroupLayout(pane);
    pane.setLayout(simpleAddQuestion);
    simpleAddQuestion.setAutoCreateGaps(true);
    simpleAddQuestion.setAutoCreateContainerGaps(true);

    // Title
    JLabel title = new JLabel("Add a Question");

    // Class
    JLabel className = new JLabel("Class: ");
    String[] classes = { "-- none selected --", "CPE 101", "Add a class..." };
    JComboBox<String> classList = new JComboBox<String>(classes);
    setMaxSize(classList);

    // Difficulty
    JLabel difficulty = new JLabel("Difficulty: ");
    JRadioButton easiest = new JRadioButton("1");
    JRadioButton easy = new JRadioButton("2");
    JRadioButton medium = new JRadioButton("3");
    JRadioButton hard = new JRadioButton("4");
    JRadioButton hardest = new JRadioButton("5");

    // Estimated Time
    JLabel estTime = new JLabel("Estimated Time: ");
    JTextField time = new JTextField();
    String[] tOptionList = { "minutes", "hours", "days" };
    JComboBox<String> timeOptions = new JComboBox<String>(tOptionList);
    setMaxSize(time, 50, 25);
    setMaxSize(timeOptions);

    // Question Type
    JLabel type = new JLabel("Type: ");
    String[] typeList = { NONE, TRUEFALSE, "Mult. Choice", "Mult. Response",
        "Matching", FILLINTHEBLANK, SHORTANSWER, "Long Answer", "Code" };
    JComboBox<String> typeOptions = new JComboBox<String>(typeList);
    typeOptions.setEditable(false);
    typeOptions.addItemListener(this);
    setMaxSize(typeOptions);
    
  //Create the "cards".
    JPanel card0 = new JPanel(new CardLayout());
    JPanel card1 = new TrueFalseQuestion();
    JPanel card2 = new FillInTheBlankQuestion();
    JPanel card3 = new ShortAnswerQuestion();
    JPanel card4 = new LongAnswerQuestion();
     
    //Create the panel that contains the "cards".
    cards = new JPanel(new CardLayout());
    cards.add(card0, NONE);
    cards.add(card1, TRUEFALSE);
    cards.add(card2, FILLINTHEBLANK);
    cards.add(card3, SHORTANSWER);
    cards.add(card4, LONGANSWER);

    // Horizontal Formatting
    simpleAddQuestion.setHorizontalGroup(simpleAddQuestion
        .createSequentialGroup()
        .addGroup(
            simpleAddQuestion.createParallelGroup(LEADING).addComponent(title)
                .addComponent(className).addComponent(difficulty).addComponent(estTime)
                .addComponent(type))
        .addGroup(
            simpleAddQuestion
                .createParallelGroup(LEADING)
                .addComponent(classList)
                .addGroup(
                    simpleAddQuestion.createSequentialGroup().addComponent(easiest)
                        .addComponent(easy).addComponent(medium).addComponent(hard)
                        .addComponent(hardest))
                .addGroup(
                    simpleAddQuestion.createSequentialGroup().addComponent(time)
                        .addComponent(timeOptions))
                .addComponent(typeOptions)
                .addGroup(
                    simpleAddQuestion.createSequentialGroup().addComponent(arg[0])
                        .addComponent(arg[1]))
                        .addComponent(cards)));

    // Vertical Formatting
    simpleAddQuestion.setVerticalGroup(simpleAddQuestion
        .createSequentialGroup()
        .addComponent(title)
        .addGroup(
            simpleAddQuestion.createParallelGroup(LEADING).addComponent(className)
                .addComponent(classList))
        .addGroup(
            simpleAddQuestion.createParallelGroup(LEADING).addComponent(difficulty)
                .addComponent(easiest).addComponent(easy).addComponent(medium).addComponent(hard)
                .addComponent(hardest))
        .addGroup(
            simpleAddQuestion.createParallelGroup(LEADING).addComponent(estTime).addComponent(time)
                .addComponent(timeOptions))
        .addGroup(
            simpleAddQuestion.createParallelGroup(LEADING).addComponent(type)
                .addComponent(typeOptions))
        .addGroup(
            simpleAddQuestion.createParallelGroup(LEADING).addComponent(arg[0])
                .addComponent(arg[1]))
    	.addComponent(cards));
  }

  private void setMaxSize(JComponent jc) {
    Dimension max = jc.getMaximumSize();
    Dimension pref = jc.getPreferredSize();
    max.height = pref.height;
    max.width = pref.width;
    jc.setMaximumSize(max);
  }

  private void setMaxSize(JComponent jc, int x, int y) {
    Dimension max = jc.getMaximumSize();
    max.height = y;
    max.width = x;
    jc.setMaximumSize(max);
  }

@Override
public void itemStateChanged(ItemEvent evt) {
    CardLayout cl = (CardLayout)(cards.getLayout());
    cl.show(cards, (String)evt.getItem());
}
}
