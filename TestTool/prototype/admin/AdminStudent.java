package admin;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import create.MenuBarFactory;

public class AdminStudent extends JFrame {

  private static final long serialVersionUID = 1L;
  private JPanel controlPanel;
  private JPanel flowPanel;
  private JPanel boxPanel;

  public AdminStudent() {
    initUI();
  }

  private void initUI() {
    setTitle("Manage Students");
    setSize(700, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setJMenuBar(MenuBarFactory.createMenu(this));
    controlPanel = new JPanel();
    controlPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    flowPanel = new JPanel();
    flowPanel.setLayout(new GridLayout(5, 1));
    boxPanel = new JPanel();
    boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.Y_AXIS));
    controlPanel.add(flowPanel);
    controlPanel.add(boxPanel);
    add(controlPanel);

    createInputs();
    createTable();
    createButtons();
  }

  private void createInputs() {
    JLabel classLabel = new JLabel("Class:", JLabel.CENTER);
    JLabel sectionLabel = new JLabel("Section:", JLabel.CENTER);
    JLabel firstNameLabel = new JLabel("First Name:", JLabel.CENTER);
    JLabel lastNameLabel = new JLabel("Last Name:", JLabel.CENTER);
    JLabel idLabel = new JLabel("EMPL ID:", JLabel.CENTER);
    final JTextField classText = new JTextField(8);
    final JTextField sectionText = new JTextField(8);
    final JTextField firstNameText = new JTextField(8);
    final JTextField lastNameText = new JTextField(8);
    final JTextField idText = new JTextField(8);

    flowPanel.add(classLabel);
    flowPanel.add(classText);
    flowPanel.add(sectionLabel);
    flowPanel.add(sectionText);
    flowPanel.add(firstNameLabel);
    flowPanel.add(firstNameText);
    flowPanel.add(lastNameLabel);
    flowPanel.add(lastNameText);
    flowPanel.add(idLabel);
    flowPanel.add(idText);
  }

  private void createTable() {
    String[] columnNames = { "Enrolled Students", "Last", "First", "EMPL ID" };
    Object[][] data = { { "1", "Lee", "Ryan", "000000001" } };

    JTable table = new JTable(data, columnNames);
    JScrollPane scrollPane = new JScrollPane(table);

    add(scrollPane, BorderLayout.LINE_END);
  }

  private void createButtons() {
    JButton addButton = new JButton("Add");
    setMaxSize(addButton);
    addButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        System.out.println("Added Student...");
        // System.exit(0);
      }
    });

    JButton deleteButton = new JButton("Delete");
    setMaxSize(deleteButton);
    deleteButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        System.out.println("Student Deleted...");
        // System.exit(0);
      }
    });

    JPanel buttonPanel = new JPanel();
    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
    buttonPanel.add(addButton);
    buttonPanel.add(deleteButton);
    add(buttonPanel, BorderLayout.SOUTH);
  }

  private void setMaxSize(JComponent jc) {
    Dimension max = jc.getMaximumSize();
    Dimension pref = jc.getPreferredSize();
    max.height = pref.height;
    max.width = pref.width;
    jc.setMaximumSize(max);
  }

}
