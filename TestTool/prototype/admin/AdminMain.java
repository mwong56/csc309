package admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import create.MenuBarFactory;

public class AdminMain extends JFrame {

  private static final long serialVersionUID = 1L;

  public AdminMain() {
    setTitle("Admin");
    setSize(700, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setJMenuBar(MenuBarFactory.createMenu(this));
    setLayout(new java.awt.GridLayout(0, 1));
    createButtons();
    setVisible(true);
  }

  private void createButtons() {
    JButton adminInstructor = new javax.swing.JButton();
    JButton adminStudent = new javax.swing.JButton();
    JButton adminReleaseTest = new javax.swing.JButton();

    adminInstructor.setText("Manage Instructors");
    adminInstructor.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        AdminInstructor ex = new AdminInstructor();
        ex.setVisible(true);
        setVisible(false);
      }
    });
    add(adminInstructor);

    adminStudent.setText("Manage Students");
    adminStudent.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        AdminStudent hs = new AdminStudent();
        hs.setVisible(true);
        setVisible(false);
      }
    });
    add(adminStudent);

    adminReleaseTest.setText("Release Test");
    adminReleaseTest.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        AdminReleaseTest hs = new AdminReleaseTest();
        hs.setVisible(true);
        setVisible(false);
      }
    });
    add(adminReleaseTest);
  }
}
