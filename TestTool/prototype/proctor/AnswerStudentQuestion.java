package proctor;
import static javax.swing.GroupLayout.Alignment.LEADING;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import create.MenuBarFactory;

public class AnswerStudentQuestion extends JFrame {

  private static final long serialVersionUID = 1L;
  private String question;
  private String answer;

  public AnswerStudentQuestion(String question, String answer) {
    this.question = question;
    this.answer = answer;
    initUI();
  }

  private void initUI() {
    setTitle("Proctor Test");
    setSize(700, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setJMenuBar(MenuBarFactory.createMenu(this));

    GroupLayout layout = new GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);

    JLabel label = new JLabel();
    label.setText("Answer Question");

    JLabel label2 = new JLabel();
    label2.setText(question);

    JTextField txtField = new JTextField();
    txtField.setText(answer);

    JButton back = new JButton();
    back.setText("Back");
    back.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        ProctorTest test = new ProctorTest();
        test.setVisible(true);
        setVisible(false);
      }
    });

    JButton submit = new JButton();
    submit.setText("Submit");
    submit.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        ProctorTest test = new ProctorTest();
        test.setVisible(true);
        setVisible(false);
      }
    });

    layout.setHorizontalGroup(layout
        .createSequentialGroup()
        .addGroup(
            layout.createParallelGroup(LEADING).addComponent(label).addGap(25).addComponent(label2)
                .addGap(25).addComponent(txtField).addGap(25)).addComponent(back)
        .addComponent(submit));

    layout
        .setVerticalGroup(layout
            .createSequentialGroup()
            .addComponent(label)
            .addGap(25)
            .addComponent(label2)
            .addGap(25)
            .addComponent(txtField)
            .addGap(25)
            .addGroup(
                layout.createParallelGroup(LEADING).addComponent(back).addComponent(submit)
                    .addGap(25)));

  }
}
