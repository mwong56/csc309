package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Main extends JFrame {

  private static final long serialVersionUID = 1L;

  public static void main(String[] args) {
    Main home = new Main();
    home.setVisible(true);
  }

  public Main() {
    setTitle("Test Tool");
    setSize(700, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setLayout(new java.awt.GridLayout(0, 1));
    createButtons();
  }

  private void createButtons() {
    JButton professor = new javax.swing.JButton();
    JButton student = new javax.swing.JButton();

    professor.setText("Professor");
    professor.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        HomeProfessor ex = new HomeProfessor();
        ex.setVisible(true);
        setVisible(false);
      }
    });
    add(professor);

    student.setText("Student");
    student.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        HomeStudent hs = new HomeStudent();
        hs.setVisible(true);
        setVisible(false);
      }
    });
    add(student);

  }
}
