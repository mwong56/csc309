package tt.model.student;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Test;

import tt.model.Course;
import tt.model.professor.Professor;
import tt.model.professor.ProfessorDB;
import tt.model.professor.proctor.StudentQuestion;

/**
 * Class StudentTest is the companion testing class for Student. It
 * implements the following module test plan:
 * 
 * <pre>
 * Phase 1: Unit test the constructor.
 * 
 * </pre>
 * 
 * @author Dat Tran
 */
public class StudentTest extends Student {

    /**
     * Empty constructor, needed to placate the compiler, since parent Student
     * constructor takes five arguments.
     */
    public StudentTest() {
        super(null, null, null, null, 0);
    }
   
    /*-*
     * Individual unit testing methods for constructors
     */

    /**
     * Unit test the constructor by building one Student object. 
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   student.fields  same non-null value    non-null value
     *          == non-null
     *   2   student.fields  null                   null
     *          == null
     */
    @Test
    public void testStudent1() {
        /**
         * Case 1
         */
        Student student = new Student("Dat", "Tran",
                "dattran", "abcde12345", 1);
        assertEquals(student.firstName, "Dat");
        assertEquals(student.lastName, "Tran");
        assertEquals(student.username, "dattran");
        assertEquals(student.password, "abcde12345");
        assertEquals(student.id, 1);
        
        /**
         * Case 2
         */
        Student student1 = new Student(null, null, null, null, 0);
        assertEquals(student1.firstName, null);
        assertEquals(student1.lastName, null);
        assertEquals(student1.username, null);
        assertEquals(student1.password, null);
        assertEquals(student1.id, 0);
    }
    
    /**
     * Unit test the constructor by building one Student object. 
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   student.fields  same non-null value    non-null value
     *          == non-null
     *   2   student.fields  null                   null
     *          == null
     */
    @Test
    public void testStudent2() {
        /**
         * Case 1
         */
        ArrayList<Course> courses = new ArrayList<Course>();
        Course c1 = new Course("CPE", 308, 1, 1);
        Course c2 = new Course("CPE", 309, 1, 2);
        courses.add(c1);
        courses.add(c2);
        Student student = new Student("Dat", "Tran",
                "dattran", "abcde12345", 1, courses);
        assertEquals(student.firstName, "Dat");
        assertEquals(student.lastName, "Tran");
        assertEquals(student.username, "dattran");
        assertEquals(student.password, "abcde12345");
        assertEquals(student.id, 1);
        assertEquals(student.courses, courses);
        
        /**
         * Case 2
         */
        Student student1 = new Student(null, null, null, null, 0);
        assertEquals(student1.firstName, null);
        assertEquals(student1.lastName, null);
        assertEquals(student1.username, null);
        assertEquals(student1.password, null);
        assertEquals(student1.id, 0);
    }
    
    /*-*
     * Individual unit testing methods for member methods
     */
    
    /**
     * Unit test the getters by constructing a Student object and then
     * checking to see if get values return appropriate data 
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   student.fields  same non-null value    non-null value
     *          == non-null
     */
    @Test
    public void testGetters() {
        /**
         * Case 1
         */
        ArrayList<Course> courses = new ArrayList<Course>();
        Course c1 = new Course("CPE", 308, 1, 1);
        Course c2 = new Course("CPE", 309, 1, 2);
        courses.add(c1);
        courses.add(c2);
        Student student = new Student("Dat", "Tran",
                "dattran", "abcde12345", 1, courses);
        assertEquals(student.getFirstName(), "Dat");
        assertEquals(student.getLastName(), "Tran");
        assertEquals(student.getUsername(), "dattran");
        assertEquals(student.getId(), 1);
        assertEquals(student.getCourses(), courses);
    }
    
    
    
    /**
     * Unit test the askStudentQuestion by checking if returned StudentQuestion
     * is in the StudentQuestions list
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   Non-empty string    questions       existent student
     *   
     **/
    @Test
    public void testAskStudentQuestion() {
        Student student = new Student("Dat", "Tran",
                "dattran", "abcde12345", 12345);
        //StudentQuestion sq = student.askQuestion("Test"); 
        //assertEquals(student.questions.get(questions.size() - 1), sq);
    }
}
