package tt.model.professor.questions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CodeQuestionTest extends CodeQuestion {

    public CodeQuestionTest() {
        super("CPE 101", 5, "3", "C", "",
                "Java", "");
    }
    
    /*-*
     * Individual unit testing methods for member methods
     */

    /**
     * Unit test the constructor by building one Question object. The other Question 
     * object constructor will be removed in a later update.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal Case      Proper init done   
     *
     */
    @Test
    public void testQuestion() {
        CodeQuestion q = new CodeQuestion("CPE 101", 3, "45", "MC", "Hello?", "Java", "");
        assertEquals(q.className, "CPE 101");
        assertEquals(q.difficulty, 3);
        assertEquals(q.timeString, "45");
        assertEquals(q.type, "MC");
        assertEquals(q.prompt, "Hello?");
        assertEquals(q.language, "Java");
        assertEquals(q.script, "");
    }
    
    /**
     * Unit test validate()
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      ""
     *
     */
    @Test
    public void testValidate() {
        String noLanguage = "No language selected!\n";
        
        CodeQuestion q = new CodeQuestion("CPE 101", 3, "45", "MC", "Hello?", "", "");
        assertEquals(q.validate(), noLanguage);
        
        q = new CodeQuestion("CPE 101", 3, "45", "MC", "Hello?", null, "");
        assertEquals(q.validate(), noLanguage);
    }
    
    /**
     * Unit test getters
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      Correct retrieval
     *
     */
    @Test
    public void testGetters() {
        CodeQuestion q = new CodeQuestion("CPE 101", 3, "45", "MC", "Hello?", "", "");
        
        assertEquals(q.getLanguage(), q.language);
        assertEquals(q.getScript(), q.script);
    }

}
