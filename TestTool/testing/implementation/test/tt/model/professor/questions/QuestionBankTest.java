package tt.model.professor.questions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tt.MainApp;
import tt.model.professor.Professor;

/**
 * Class QuestionBankTest is the companion testing class for QuestionBank. It
 * implements the following module test plan:
 * 
 * <pre>
 * Phase 1: Unit test the constructor.
 * 
 * Phase 2: Unit test the simple get/set methods.
 * 
 * Phase 3: Unit test add, saving the constructed results for 
 *          subsequent tests.
 *          
 * Phase 4: Unit test delete, deleting the items added in Phase 3.
 * </pre>
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 */
public class QuestionBankTest extends QuestionBank {

    /*-*
     * Individual unit testing methods for member methods
     */

    /**
     * Unit test the constructor by building one Question object. The other Question 
     * object constructor will be removed in a later update.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal Case      Proper init done   
     *   2      Missing one      Proper init done
     *   3      Missing many     Proper init done
     *   4      null             Proper init done
     *
     */
    @Test
    public void testQuestionBank() {
        QuestionBank q = new QuestionBank();
        q = new QuestionBank("gfisher");
        q = new QuestionBank("testBank");
    }
    
    /**
     * Unit test the addQuestion function. It doesn't do validation as it should 
     * be checked beforehand
     */
    @Test
    public void testAddQuestion() {
        QuestionBank q = new QuestionBank("testBank");
        
        q.addQuestion(new Question("", 0, 0, "", ""));
        q.addQuestion(new Question("a", 0, 0, "a", "a"));
        q.addQuestion(new Question("b", 0, 0, "b", "b"));
    }
    
    /**
     * Unit test the updateQuestion function. It doesn't do validation as it should 
     * be checked beforehand
     */
    @Test
    public void testUpdateQuestion() {
        QuestionBank q = new QuestionBank("testBank");
        
        q.updateQuestion(new Question("", 0, 0, "", ""), new Question("CPE101", 0, 0, "sda", "da"));
    }
    
    /**
     * Unit test the delete function.
     */
    @Test
    public void testDelete() {
        QuestionBank q = new QuestionBank("testBank");
        
        q.delete(0);
    }
    
    /**
     * Unit test getters
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      Correct retrieval
     *
     */
    @Test
    public void testGetters() {
        QuestionBank q = new QuestionBank("testBank");
        q.addQuestion(new Question("c", 0, 0, "c", "c"));
        q.addQuestion(new Question("d", 0, 0, "d", "d"));
        
        assertEquals(q.get(0), q.questionData.get(0));
        assertEquals(q.getClassData(), q.classData);
        assertEquals(q.getQuestionData(), q.questionData);
    }
    
    /**
     * Unit test setters
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      Correct retrieval
     *
     */
    @Test
    public void testSetters() {
        QuestionBank q = new QuestionBank("testBank");
        MainApp app = new MainApp();
        app.setProfessor(new Professor("", "", "", "", 0));
        q.setMainApp(app);
        assertEquals(q.mainApp, app);
    }

}
