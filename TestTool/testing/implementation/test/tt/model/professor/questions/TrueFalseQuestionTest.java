package tt.model.professor.questions;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Class QuestionTypeTest is the companion testing class for QuestionType. It
 * implements the following module test plan:
 * 
 * <pre>
 * Phase 1: Unit test the constructor.
 * 
 * Phase 2: Unit test the simple get/set methods.
 * 
 * Phase 3: Unit test add, saving the constructed results for 
 *          subsequent tests.
 *          
 * Phase 4: Unit test delete, deleting the items added in Phase 3.
 * </pre>
 * 
 * @author Mandy Chan (mchan9993@gmail.com)
 */
public class TrueFalseQuestionTest extends TrueFalseQuestion {

    public TrueFalseQuestionTest() {
        super("CPE101", 5, "1", "TF", "Sup", "");
    }

    /*-*
     * Individual unit testing methods for member methods
     */

    /**
     * Unit test the constructor by building one Question object. The other Question 
     * object constructor will be removed in a later update.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal Case      Proper init done   
     *   2      Missing one      Proper init done
     *   3      Missing many     Proper init done
     *   4      null             Proper init done
     *
     */
    @Test
    public void testQuestion() {
        Question q = new TrueFalseQuestion("CPE 101", 3, "45", "MC", "Hello?", "");
        
        assertEquals(q.className, "CPE 101");
        assertEquals(q.difficulty, 3);
        assertEquals(q.timeString, "45");
        assertEquals(q.type, "MC");
        assertEquals(q.prompt, "Hello?");
        assertEquals(q.answer, "");
    }
    
    /**
     * Unit test validate()
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      No correct ans   No Correct Answer error    
     *   2      True/False       ""   
     *
     */
    @Test
    public void testValidate() {
        String noCorrectAns = "No correct answer selected!\n";
        
        TrueFalseQuestion q = new TrueFalseQuestion("CPE 101", 3, "45", "MC", "Hello?", null);
        assertEquals(q.validate(), noCorrectAns);
        
        q = new TrueFalseQuestion("CPE 101", 3, "45", "MC", "Hello?", "True");
        assertEquals(q.validate(), "");
        
        q = new TrueFalseQuestion("CPE 101", 3, "45", "MC", "Hello?", "False");
        assertEquals(q.validate(), "");
    }

    
    /**
     * Unit test strToBool()
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      null             false    
     *   2      True/False       true   
     *
     */
    @Test
    public void testStrToBool() {
        String noCorrectAns = "No correct answer selected!\n";
        
        TrueFalseQuestion q = new TrueFalseQuestion("CPE 101", 3, "45", "MC", "Hello?", null);
        assertFalse(q.strToBool());
        
        q = new TrueFalseQuestion("CPE 101", 3, "45", "MC", "Hello?", "True");
        assertTrue(q.strToBool());
    }
    
    /**
     * Unit test getters
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      Normal case      Correct retrieval
     *
     */
    @Test
    public void testGetters() {
        TrueFalseQuestion q = new TrueFalseQuestion("CPE 101", 3, "45", "MC", "Hello?", "True");
        
        assertEquals(q.getCorrectAnswer(), q.correctAnswer);
    }
}
