package tt.model.professor.admin;

//import java.util.*;
import static org.junit.Assert.*;

import org.junit.Test;

/****
 * 
 * @author Ryan Lee
 *
 * Class AdminTest is the companion testing class for class Admin.
 * It implements the following module test plan:
 * 
 *     Phase 1: Unit test the constructor.
 *                                                                      
 *     Phase 2: Unit test the simple access method getType.
 *                                                                      
 *     Phase 3: Unit test the setType and saveSettings methods.
 *                                                                      
 *     Phase 4: Repeat phases 1 through 3.
 */

public class AdminTest extends Admin{
    protected Admin admin;
    
    /**
     * Empty constructor, needed to placate the compiler, since parent Schedule
     * constructor takes two arguments.
     */
    protected AdminTest() {
        super(null);
    }
    
    /*-*
     * Individual unit testing methods for member methods
     */
    
    /**
     * Unit test the constructor by building two Admin objects.  No further
     * constructor testing is necessary since only two Admin objects are ever
     * constructed in the TestTool system.
     *                                                                    
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      null                                Null case
     *   2      "In Class"                          Non-null case
     *
     */
    @Test
    protected void testAdmin() {
        admin = new Admin();
        assertNotNull("should not be null", admin);
        admin = new Admin("In Class");
        assertNotNull("should not be null", admin);
    }
    
    /**
     * Unit test getType by calling getType on an admin with a
     * null and non-null categories field.
     *                                                                    
     *  Test
     *  Case    Input                   Output          Remarks
     * ====================================================================
     *   1      admin.testType                          Null case
     *            == null
     *
     *   2      admin.testType                          Non-null case
     *            == non-null      
     */
    @Test
    protected void testGetType() {
        admin = new Admin();
        assertNull("should be null", admin.getType());
        admin = new Admin("In Class");
        assertEquals(admin.getType(), "In Class");
    }
    
    /**
     * Unit test setType by checking to see if the testType changes after
     * setType is called.
     *
     *  Test
     *  Case    Input                Output                 Remarks
     * ====================================================================
     *   1      admin.testType                              Null case
     *           == null
     *   2      "Practice"                                  Change testType
     */
    @Test
    protected void testSetType() {
        admin = new Admin();
        assertNull("should be null", admin.getType());
        admin.setType("Practice");
        assertNotNull("should not be null", admin.getType());
        assertEquals(admin.getType(), "Practice");
    }
}
