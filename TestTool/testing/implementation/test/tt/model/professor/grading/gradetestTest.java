package tt.model.professor.grading;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import static org.junit.Assert.*;

import org.junit.Test;

import tt.model.professor.Professor;
import tt.model.professor.admin.Admin;
import tt.model.student.Student;
import tt.model.student.take.FinishedTest;

/**
 * Class QuestionBankTest is the companion testing class for QuestionBank. It
 * implements the following module test plan:
 * 
 * <pre>
 * Phase 1: Unit test the constructor.
 * 
 * Phase 2: Unit test the simple get/set methods.
 * 
 * Phase 3: Unit test add, saving the constructed results for 
 *          subsequent tests.
 *          
 * Phase 4: Unit test delete, deleting the items added in Phase 3.
 * </pre>
 * 
 * @author Ka Tong
 */
public class gradetestTest extends gradetest{
    
    public gradetest gradetest;
    /**
     * constructor, needed to located the compiler, since parent Schedule
     * constructor takes two arguments.
     */
    @Test
    public void testgradetest()
    {
        gradetest = new gradetest();
        assertNotNull("should not be null", gradetest);
        ObservableList<FinishedTest> finish = gradetest.getTest();
        assertNotNull("should not be null", finish);
        ObservableList<Student> student = gradetest.getStudent();
        assertNotNull("should not be null", student);
    }
    /*
    public gradetestTest()
    {
        super(null, null, null);
    }
   
    
    @Test
    public void testgradetest() {
        gradetest = new gradetest();
        assertNotNull("should not be null", gradetest);
        gradetest = new gradetest("Question 1", "Khosmood!!!!", "Foaadd!!!");
        assertNotNull("should not be null", gradetest);
    }
    
    public StringProperty questionProperty() {
        return question;
    }
    @Test
    public void testQuestionProperty() {
        gradetest = new gradetest();
        assertNull("should be null", gradetest.questionProperty().get());
        gradetest = new gradetest("Hot Dog", "final", "CPE309");
        //assertEquals(gradetest.questionProperty(), );
    }
    @Test
    public void testQuestionTest() {
        gradetest = new gradetest();
        assertNull("should be null", gradetest.questionTest());
        gradetest = new gradetest("Fisher!!!!", "final", "CPE309");
        assertEquals(gradetest.questionTest(), "Fisher!!!!");
    }
    
    @Test
    public void teststudentAnswer() {
        gradetest = new gradetest();
        assertNull("should be null", gradetest.studentAnswer());
        gradetest = new gradetest("Fisher!!!!", "final", "CPE309");
        assertEquals(gradetest.studentAnswer(), "final");
    }
    
    @Test
    public void testRealProperty() {
        gradetest = new gradetest();
        assertNull("should be null", gradetest.realAnswer());
        gradetest = new gradetest("Fisher!!!!", "final", "CPE309");
        assertEquals(gradetest.realAnswer(), "CPE309");
    }
    
    @Test
    public void testReturnTest()
    {
        
    }
    
     */
}
