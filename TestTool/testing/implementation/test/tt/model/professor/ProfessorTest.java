package tt.model.professor;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.*;

import tt.model.Course;
import tt.model.professor.proctor.StudentQuestion;

/**
 * Class ProfessorTest is the companion testing class for Professor. It
 * implements the following module test plan:
 * 
 * <pre>
 * Phase 1: Unit test the constructor.
 * Phase 2: Unit test the getters.
 * 
 * </pre>
 * 
 * @author Dat Tran
 */

public class ProfessorTest extends Professor {
    
    /**
     * Empty constructor, needed to placate the compiler, since parent Professor
     * constructor takes five arguments.
     */
    public ProfessorTest() {
        super(null, null, null, null, 0);
    }

    /*-*
     * Individual unit testing methods for constructors
     */

    /**
     * Unit test the constructor with five parameters
     * (without List of Courses) by building one Professor object. 
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   professor.fields  same non-null value    non-null value
     *          == non-null
     *   2   professor.fields  null                   null
     *          == null
     */
    @Test
    public void testProfessor1() {
        /**
         * Case 1
         */
        Professor prof = new Professor("Gene", "Fisher", "gfisher", "team1isthebest", 1);
        assertEquals(prof.firstName, "Gene");
        assertEquals(prof.lastName, "Fisher");
        assertEquals(prof.username, "gfisher");
        assertEquals(prof.password, "team1isthebest");
        assertEquals(prof.id, 1);
        
        /**
         * Case 2
         */
        Professor prof1 = new Professor(null, null, null, null, 0);
        assertEquals(prof1.firstName, null);
        assertEquals(prof1.lastName, null);
        assertEquals(prof1.username, null);
        assertEquals(prof1.password, null);
        assertEquals(prof1.id, 0);
    }
    
    
    /**
     * Unit test the constructor with six parameters 
     * (with List of Courses) by building one Professor object. 
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   professor.fields  same non-null value    non-null value
     *          == non-null
     *   2   professor.fields  null                   null
     *          == null
     */
    @Test
    public void testProfessor2() {
        /**
         * Case 1
         */
        ArrayList<Course> courses = new ArrayList<Course>();
        Course course1 = new Course("CPE", 308, 1, 1);
        Course course2 = new Course("CPE", 308, 2, 2);
        Course course3 = new Course("CPE", 309, 1, 3);
        courses.add(course1);
        courses.add(course2);
        courses.add(course3);
        Professor prof = new Professor("Gene", "Fisher", "gfisher", "team1isthebest", 1, courses);
        assertEquals(prof.firstName, "Gene");
        assertEquals(prof.lastName, "Fisher");
        assertEquals(prof.username, "gfisher");
        assertEquals(prof.password, "team1isthebest");
        assertEquals(prof.id, 1);
        assertEquals(prof.courses.get(0), course1);
        assertEquals(prof.courses.get(1), course2);
        assertEquals(prof.courses.get(2), course3);
        
        /**
         * Case 2
         */
        Professor prof1 = new Professor(null, null, null, null, 0, null);
        assertEquals(prof1.firstName, null);
        assertEquals(prof1.lastName, null);
        assertEquals(prof1.username, null);
        assertEquals(prof1.password, null);
        assertEquals(prof1.id, 0);
        assertEquals(prof1.courses, null);
    }

    /*-*
     * Individual unit testing methods for get methods
     */

    /**
     * Unit test the getter for the professor's first name 
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   professor.getFirstName()  same non-null value    non-null value
     *          == non-null
     *   2   professor.getFirstName()  null                   null
     *          == null
     */
    @Test
    public void testGetFirstName() {
        /**
         * Case 1
         */
        Professor prof = new Professor("Gene", "Fisher", "gfisher", "team1isthebest", 1);
        assertEquals(prof.getFirstName(), "Gene");
  

        /**
         * Case 2
         */
        Professor prof1 = new Professor(null, null, null, null, 0);
        assertEquals(prof1.getFirstName(), null);
    }
    
    /**
     * Unit test the getter for the professor's last name 
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   professor.getLastName()  same non-null value    non-null value
     *          == non-null
     *   2   professor.getLastName()  null                   null
     *          == null
     */
    @Test
    public void testGetLastName() {
        /**
         * Case 1
         */
        Professor prof = new Professor("Gene", "Fisher", "gfisher", "team1isthebest", 1);
        assertEquals(prof.getLastName(), "Fisher");
  

        /**
         * Case 2
         */
        Professor prof1 = new Professor(null, null, null, null, 0);
        assertEquals(prof1.getLastName(), null);
    }
    
    /**
     * Unit test the getter for the professor's username 
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   professor.getUsername()  same non-null value    non-null value
     *          == non-null
     *   2   professor.getUsername()  null                   null
     *          == null
     */
    @Test
    public void testGetUsername() {
        /**
         * Case 1
         */
        Professor prof = new Professor("Gene", "Fisher", "gfisher", "team1isthebest", 1);
        assertEquals(prof.getUsername(), "gfisher");
  

        /**
         * Case 2
         */
        Professor prof1 = new Professor(null, null, null, null, 0);
        assertEquals(prof1.getUsername(), null);
    }
    
    /**
     * Unit test the getter for the professor's ID 
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   professor.getId()  same non-null value    non-null value
     *          == non-null
     *   2   professor.getId()  null                   null
     *          == null
     */
    @Test
    public void testGetId() {
        /**
         * Case 1
         */
        Professor prof = new Professor("Gene", "Fisher", "gfisher", "team1isthebest", 1);
        assertEquals(prof.getId(), 1);
  

        /**
         * Case 2
         */

        try {
            Professor prof1 = new Professor(null, null, null, null, (Long)null);
            prof1.getId(); 
        } catch (NullPointerException e) {
            assertTrue(true);
        }
    }
    
    
    /**
     * Unit test the getter for the professor's courses.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   professor.courses  same non-null value    non-null value
     *          == non-null
     *   2   professor.courses  null                   null
     *          == null
     */
    @Test
    public void testGetCourses() {
        /**
         * Case 1
         */
        ArrayList<Course> courses = new ArrayList<Course>();
        Course course1 = new Course("CPE", 308, 1, 1);
        Course course2 = new Course("CPE", 308, 2, 2);
        Course course3 = new Course("CPE", 309, 1, 3);
        courses.add(course1);
        courses.add(course2);
        courses.add(course3);
        Professor prof = new Professor("Gene", "Fisher", "gfisher", "team1isthebest", 1, courses);
        assertEquals(prof.getCourses().get(0), course1);
        assertEquals(prof.getCourses().get(1), course2);
        assertEquals(prof.getCourses().get(2), course3);
        
        /**
         * Case 2
         */
        Professor prof1 = new Professor(null, null, null, null, 0, null);
        assertEquals(prof1.getCourses(), null);
    }
    
    /**
     * Unit test the getter for the professor's course abbreviations.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   professor.courses  same non-null value    non-null value
     *          == non-null
     *   2   professor.courses  null                   null
     *          == null
     */
    @Test
    public void testGetCourseAbbrs() {
        /**
         * Case 1
         */
        ArrayList<Course> courses = new ArrayList<Course>();
        Course course1 = new Course("CPE", 308, 1, 1);
        Course course2 = new Course("CPE", 308, 2, 2);
        Course course3 = new Course("CPE", 309, 1, 3);
        courses.add(course1);
        courses.add(course2);
        courses.add(course3);
        Professor prof = new Professor("Gene", "Fisher", "gfisher", "team1isthebest", 1, courses);
        assertEquals(prof.getCourseAbbrs().get(0), "CPE");
        
        /**
         * Case 2
         */
        Professor prof1 = new Professor(null, null, null, null, 0, null);
        assertEquals(prof1.getCourseAbbrs().size(), 0);
    }
    
    /**
     * Unit test the getter for the professor's courses numbers.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   professor.courses  same non-null value    non-null value
     *          == non-null
     *   2   professor.courses  null                   null
     *          == null
     */
    @Test
    public void testGetDistinctCourses() {
        /**
         * Case 1
         */
        ArrayList<Course> courses = new ArrayList<Course>();
        Course course1 = new Course("CPE", 308, 1, 1);
        Course course2 = new Course("CPE", 308, 2, 2);
        Course course3 = new Course("CPE", 309, 1, 3);
        courses.add(course1);
        courses.add(course2);
        courses.add(course3);
        Professor prof = new Professor("Gene", "Fisher", "gfisher", "team1isthebest", 1, courses);
        assertEquals(prof.getDistinctCourses().get(0).getCourseAbbr(), "CPE");
        assertEquals(prof.getDistinctCourses().get(0).getNumber(), 308);
        assertEquals(prof.getDistinctCourses().get(1).getCourseAbbr(), "CPE");
        assertEquals(prof.getDistinctCourses().get(1).getNumber(), 309);
        
        /**
         * Case 2
         */
        try {
            Professor prof1 = new Professor(null, null, null, null, 0, null);
            ArrayList<Course> c = prof1.getDistinctCourses();
        } catch (NullPointerException e) {
            assertTrue(true);
        }

    }
    
    /**
     * Unit test the getter for the professor's courses numbers.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   professor.courses  same non-null value    non-null value
     *          == non-null
     *   2   professor.courses  null                   null
     *          == null
     */
    @Test
    public void testGetCourseNums() {
        /**
         * Case 1
         */
        ArrayList<Course> courses = new ArrayList<Course>();
        Course course1 = new Course("CPE", 308, 1, 1);
        Course course2 = new Course("CPE", 308, 2, 2);
        Course course3 = new Course("CPE", 309, 1, 3);
        courses.add(course1);
        courses.add(course2);
        courses.add(course3);
        Professor prof = new Professor("Gene", "Fisher", "gfisher", "team1isthebest", 1, courses);
        assertEquals(prof.getCourseNums().get(0), (Integer)308);
        assertEquals(prof.getCourseNums().get(1), (Integer)309);
        
        /**
         * Case 2
         */
        try {
            Professor prof1 = new Professor(null, null, null, null, 0, null);
            ArrayList<Integer> c = prof1.getCourseNums();
        } catch (NullPointerException e) {
            assertTrue(true);
        }
    }
    
    
    /**
     * Unit test the getter for the professor's courses sections.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   professor.courses  same non-null value    non-null value
     *          == non-null
     *   2   professor.courses  null                   null
     *          == null
     */
    @Test
    public void testGetCourseSections() {
        /**
         * Case 1
         */
        ArrayList<Course> courses = new ArrayList<Course>();
        Course course1 = new Course("CPE", 308, 1, 1);
        Course course2 = new Course("CPE", 308, 2, 2);
        Course course3 = new Course("CPE", 309, 1, 3);
        courses.add(course1);
        courses.add(course2);
        courses.add(course3);
        Professor prof = new Professor("Gene", "Fisher", "gfisher", "team1isthebest", 1, courses);
        assertEquals(prof.getSections(course1).get(0), (Integer)1);
        assertEquals(prof.getSections(course1).get(1), (Integer)2);
        
        /**
         * Case 2
         */
        Professor prof1 = new Professor(null, null, null, null, 0, null);
        //assertEquals(prof1.getSections(course1), null);
    }
    
    /**
     * Unit test the getter for the professor's list of student questions.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1   professor.courses  same non-null value    non-null value
     *          == non-null
     */
    @Test
    public void testGetStudentQuestions() {
        /**
         * Case 1
         */
        Professor prof = new Professor("Gene", "Fisher", "gfisher", "team1isthebest", 1);
        Course course = new Course("CPE", 308, 1, 1);
        tt.model.professor.create.Test test = new tt.model.professor.create.Test(
                course.getCourseAbbr() + course.getNumber(), "Midterm #1");
        StudentQuestion s1 = new StudentQuestion("Is this working?", course, null, "");
        ArrayList<StudentQuestion> sq1 = new ArrayList<StudentQuestion>();
        sq1.add(s1);
        assertEquals(prof.getStudentQuestions().size(), 0);
        
    }
}
