package tt.model.professor.create;

import static org.junit.Assert.assertEquals;

public class TestSettingsTest extends TestSettings {
    public TestSettingsTest() {
        super();
    }
    
    /**
     * Unit test the constructor by building one Question object. 
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      null             Proper init done   Only case
     *
     */
    public void testTestSettings() {
        TestSettings ts = new TestSettings(5, 3, 20.0, 10.0, 20.0, 10.0, 10.0, 10.0, 10.0, 10.0);
        assertEquals(ts.getAvgDiff(), "5");
        assertEquals(ts.getDiffRange(), "3");
        assertEquals(ts.getNumMC(), "0.2");
        assertEquals(ts.getNumTF(), "0.1");
        assertEquals(ts.getNumSA(), "0.2");
        assertEquals(ts.getNumCoding(), "0.1");
        assertEquals(ts.getNumLA(), "0.1");
        assertEquals(ts.getNumMR(), "0.1");
        assertEquals(ts.getNumM(), "0.1");
        assertEquals(ts.getNumFB(), "0.1");
    }
}
