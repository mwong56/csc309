package create;

/**
 * Navigates to the test settings options
 */
public abstract class TestSettingView {
	double setTime;
	int currDifficulty;
	double diffRange;
	int numMC;
	int numTF;
	int numFill;
	int numCode;
	String resources;
	
	/**
	 post:
	 // Generates a Test with TestSettingView settings applied and store in History
	 return DefaultSettings;
	 */
	abstract DefaultSettings changeToDefault();
}
