package create;

import questions.Question;
import questions.QuestionBank;


/*
 * Add question from test bank
 */
public abstract class AddQuestion {
	abstract QuestionBank viewQuestions();
	abstract Question addQuestion();
}
