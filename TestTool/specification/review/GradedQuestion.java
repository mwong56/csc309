package review;
import questions.Question;

/**
 * A single question from a student's completed test. Contains a reference to the question at hand, as well as 
 * info regarding its current grading status.
 * 
 * @author (Casey Sheehan) 
 * @version (Milestone 6)
 */
abstract class GradedQuestion
{
    Question question; //from Question.java elsewhere
    String status;
}
