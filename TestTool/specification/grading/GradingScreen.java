package grading;

/**
 * The GradingScreen displays information regarding the grading status of
 * various tests completed by students, thus representing the PROFESSOR VIEW of
 * the grading information. As of Milestone 6, this screen is now separate from
 * that of a students's, which is now considered a "ReviewScreen".
 * 
 * @author (Casey Sheehan)
 * @version (Milestone 6)
 */
abstract class GradingScreen {
  ProfessorTestList professorsList;
}
