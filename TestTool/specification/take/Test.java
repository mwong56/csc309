package take;

import java.sql.Date;
import java.sql.Time;
import java.util.Collection;

import questions.Question;

/**
 * A Test class is a collection of questions and other attributes about the test
 */
public abstract class Test {
  Collection<Question> questions; /* All the questions in the test */
  Time limit; /* How much time is given/left in the test */
  Date close; /* Determines when the test stops being open */
  int attempts; /* The number of attempts allowed */
  String title; /* The name of the test */
  String course; /* The class that this test is for */
  String professor; /* The professor who administered the test */
  String description; /* A description of the test */
  String type; /* The type of test */
  boolean isOpen; /* Controls whether the test is open or not */
  
  /*
   * A method that moves the student over to the StudentTake interface
   * 
   * <pre>
     pre:
   	  //
   	  // 
   	  //  
     post:
      //
      // The 
      //
   */
  abstract StudentTake takeTest();
}
