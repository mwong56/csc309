package questions;

/**
 * The basic question has a class name, difficulty, time, prompt.
 */
public abstract class Question {
  String className;
  int difficulty;
  int time;
  int points;
  String prompt;
}
