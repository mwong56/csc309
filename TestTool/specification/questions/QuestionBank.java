package questions;

import java.util.Collection;

/**
 * A question bank has a collection of questions.
 * 
 * The question bank has the functions addQuestion(), deleteQuestion(),
 * filterQuestion(), and search().
 */

public abstract class QuestionBank {
  Collection<Question> questions;

  /**
   * Adds the input question into this.QuestionBank.
   * 
   * @param question The question to be added
   * 
 	<pre>
	pre:
		//
		// The question isn't empty
		//
		question != null
    
   			&&
    
		//
		// The prompt isn't empty
		//
		question.prompt.length() > 0;
    
   	post:
		//
		// The given question is in this.questions.
		//
		questions'.contains(question);
   */
  abstract void addQuestion(Question question);

  /**
   * Deletes the input question if found in this.QuestionBank.
   * 
   * @param question The question to be deleted
   * 
   <pre>
    pre:
    	//
    	// The given question is in this.questions.
    	//
    	questions'.contains(question);
    
    post:
    	//
    	// The given question is removed from this.questions.
    	//
    	!questions'.contains(question);
   */
  abstract void deleteQuestion(Question question);

  /**
   * Filters this.questions to specified conditions.
   * 
   * @param className Class name the collection of questions returned must have
   * @param type Type of question the collection of questions returned must be
   * @param lowDiff The lowest difficulty each question in the collection of questions returned must be above
   * @param highDiff The highest difficulty each question in the collection of questions returned must be below
   * @param flagTime A flag indicating whether each question in the collection of questions returned must be below or above
   * @param time The duration each question in the collection of questions returned must be below (or above)
   * 
   * @return A collection of Questions that satisfies the conditions.
   	<pre>
   	pre:
   		//
   		// If className isn't null, it must be a length greater than 0.
   		//
   		if (className != null)
   			className.length() > 0
   		
   			&&
   			
   		//
   		// If type isn't null, it must be a length greater than 0.
   		//
   		if (type != null)
   			type.length() > 0
   		
   			&&
   		//
   		// If lowDiff isn't null, it must be from 1 to 5 and lower than highDiff
   		//
   		if (lowDiff != null)
   			lowDiff >= 1 && lowDiff <= 5 && lowDiff < highDiff
   			
   			&&
   		
   		//
   		// If highDiff isn't null, it must be from 1 to 5 and higher than lowDiff
   		//
   		if (highDiff != null)
   			highDiff >= 1 && highDiff <= 5 && highDiff > lowDiff
   			
   	post:
    	//
    	// A question is in the output list if and only if it is in the 
    	// input QuestionBank and the question satisfies all conditions.
    	//
    	forall ( Question q ; questions.contains(q) ; 
    		return.contains(q)
    			(if
	    			(q.className.equals(className) && q.difficulty >= lowDiff && 
	    			q.difficulty <= highDiff && flag == 0 && q.time >= time)
    			else if
    				(q.className.equals(className) && q.difficulty >= lowDiff && 
	    			q.difficulty <= highDiff && flag > 0 && q.time < time)));
   */
  abstract Collection<Question> filterQuestion(String className, String type, int lowDiff, int highDiff, int flagTime, int time);

  /**
   * Searches in this.questions for questions with a prompt containing query.
   * 
   * @param query the String that is to be found in the questions' prompts
   * @return A collection of Questions with prompts containing the query.
   * 
	<pre>
	pre:
    	//
    	// The query isn't empty.
    	//
    	(query.length() > 0)
    
	post:
    	//
    	// A question is in the output list if and only if it is in the 
    	// input QuestionBank and the question prompt contains the query.
    	//
    	forall ( Question q ; 
    		return.contains(q) iff
    			questions.contains(q) && q.prompt.contains(query));
   */
  abstract Collection<Question> search(String query);
  
  /**
   * Sorts this.questions according to sortType.
   * 
   * @param sortType The way to sort this.questions
   * @param alphanumeric Whether to sort it alpha-numerically or the other way around
   * 
	<pre>
	post:
		//
		// All the questions from this.questions is still there.
		//
		forall ( Question q ; questions.contains(q));
   */
  abstract void sort(int sortType, boolean alphanumeric);
}
