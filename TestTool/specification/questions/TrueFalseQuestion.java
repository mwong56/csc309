package questions;

/**
 * A True/False question has a boolean for the student's answer 
 * and a boolean for the correct answer.
 */
public abstract class TrueFalseQuestion extends Question {
  boolean studentAnswer;
  boolean correctAnswer;
}
