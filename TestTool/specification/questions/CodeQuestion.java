package questions;

/**
 * A code question has a String code for students to enter their answer 
 * and a String script for evaluating the student's code.
 */
public abstract class CodeQuestion extends Question {
  String code;
  String script;
}
