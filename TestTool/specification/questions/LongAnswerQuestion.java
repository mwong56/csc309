package questions;

/**
 * A long answer question has a string answer and the test-maker's answer notes.
 */
public abstract class LongAnswerQuestion extends Question {
  String longAnswer;
  String answerNotes;
}
