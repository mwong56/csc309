package proctor;

import questions.Question;

/**
 * A student question has an id, question and an answer. The answer will be
 * empty until a professor answers it.
 */
public abstract class StudentQuestion extends Question {
  int id;
  String answer;
}
