package proctor;

import java.util.Collection;

/**
 * A student has questions that they want to ask and two functions to get
 * questions and save questions. Get question and save question will interface
 * with the StudentQuestionDB.
 */
public abstract class StudentProctor {
  Collection<StudentQuestion> questions;
  StudentQuestionsDB db;

  /**
   * Populates this.questions() by calling this.db.getQuestions() and returns
   * this.questions()
   * 
   * @return
   */
  abstract Collection<StudentQuestion> getQuestions();

  /**
   * Saves all questions to this.db
   * 
   * @param questions
   *          The list of questions to be saved.
   */
  abstract void saveQuestions(Collection<StudentQuestion> questions);
}
