package admin;

/**
 * In Class test
 */
/* DUPLICATE CLASS DEFINITION
 abstract class InClass {
 boolean closedBook;
 }
 */

/**
 * Practice test class
 */
/* DUPLICATE CLASS DEFINITION
 abstract class Practice {
 boolean closedBook;
 int numTries;
 }
 */

/**
 * Time class for take home test
 */
/* DUPLICATE CLASS DEFINITION
 abstract class Time {
 int hour;
 int min;
 boolean am;
 }
 */

/**
 * Take Home test
 */
/* DUPLICATE CLASS DEFINITION
 abstract class TakeHome {
 Time startTime;
 Time endTime;
 boolean closedBook;
 int numTries;
 }
 */

/**
 * Release Test screen
 */
abstract class ReleaseTestScreen {
  // Collection<Test> tests;

  // Use In Class class to save settings to selected test
  abstract void isInClass();

  // Use Practice class to save settings to selected test
  abstract void isPractice();

  // Use TakeHome class to save settings to selected test
  abstract void isTakeHome();

  // abstract void releaseTest(Test test);
}
