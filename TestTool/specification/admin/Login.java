package admin;

/**
 * The login screen has two text fields that capture user name and password,
 * two radio buttons that capture whether the user is logging in as a student
 * or a professor and a button that captures when the user hits login.
 */
abstract class Login {
  String username;
  String password;
  boolean isStudent;
}
