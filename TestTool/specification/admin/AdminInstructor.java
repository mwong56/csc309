package admin;

import java.util.Collection;

/**
  * Instructor class that stores instructor's name, as well as the permissions
  * that the instructor has. Will determine what the instructor is able to do
  * within the program.
  */
abstract class Instructor {
  Name name;
  boolean canEditQues;
  boolean canMakeTests;
  boolean canManageGrades;
  boolean isAdmin;
}

/**
  * Houses backend data of instructor screen.
  */
abstract class AdminInstructor {
    Collection<Instructor> instructors;

    abstract void addInstructor();

    abstract void editInstructor();

    abstract void deleteInstructor();
}

abstract class Name {
  String first;
  String last;
}
