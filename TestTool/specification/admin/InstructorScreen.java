package admin;

import java.util.Collection;

/**
  * Instructor class that stores instructor's name, as well as the permissions
  * that the instructor has. Will determine what the instructor is able to do
  * within the program.
  */
/* DUPLICATE CLASS DEFINITION
abstract class Instructor {
  String name;
  boolean canEditQues;
  boolean canMakeTests;
  boolean canManageGrades;
  boolean isAdmin;
}
 */

/**
  * Houses backend data of instructor screen.
  */
abstract class InstructorScreen {
    Collection<Instructor> instructors;

    abstract void addInstructor();

    abstract void editInstructor();

    abstract void deleteInstructor();
}
