package admin;

import java.util.Collection;

/**
  * Student class that stores the student's name and id.
  */
/* DUPLICATE CLASS DEFINITION
abstract class Student {
  String name;
  long id;
}
 */

/**
  * Houses backend data of student screen.
  */
abstract class StudentScreen {
  Collection<Student> students;

  abstract void addStudent();

  abstract void deleteStudent();
}
