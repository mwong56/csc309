package admin;

//import home.NavBar;

/**
  * Main admin screen which handles redirection to subclasses.
  */
public abstract class Admin {
  AdminInstructor instructor;
  AdminStudent student;
  AdminReleaseTest releaseTest;
}
